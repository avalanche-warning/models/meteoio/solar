/******************************************************************************/
/* Copyright 2022 alpine-software.at                                          */
/******************************************************************************/
/* This is free software you can redistribute/modify under the terms of the   */
/* GNU Affero General Public License 3 or later: http://www.gnu.org/licenses  */
/******************************************************************************/
/*
 * Calculate sum of incoming shortwave radiation between two dates.
 * Michael Reisecker, 2020-12-13
 */

#include <algorithm>
#include <execution>
#include <fstream>
#include <iostream>
#include <mutex>
#include <string>
#include <vector>

#include <meteoio/MeteoIO.h>

bool check_sampling_rate(mio::IOManager &io, const mio::Date& sdate, const mio::Date& edate)
{
	std::vector< std::vector<mio::MeteoData> > md;
	io.getMeteoData(sdate, edate, md);
	const double sampling_rate = 1. / (io.getAvgSamplingRate() * 60);
	std::cout << "Average sampling rate of dataset: 1 per "
	    << sampling_rate << " min." << std::endl;
	if (sampling_rate < 0) {
		std::cerr << "[W] No valid data found for timespan." << std::endl;
		return false;
	}
	return true;
}

int main(int argc, char** argv)
{
	std::cout << "This is " << __FILE__ << std::endl;

	if (argc != 3)
		throw std::invalid_argument("Exactly two command line input arguments are expected: Start and end date.");

	//list of hours of the day the calculation is performed at:
	std::vector<int> hours{6, 8, 10, 11, 12, 13, 14, 16, 18};
	
	mio::Config cfg("input/io.ini");
	mio::IOManager io_out(cfg);
	std::cout << "Reading DEM..." << std::endl;
	mio::DEMObject dem;
	io_out.readDEM(dem); //retrieve meta data of area

	std::cout << "Checking dates..." << std::endl;
	const double TZ = cfg.get("TIME_ZONE", "Input"); //get dates according to time zone
	mio::Date sdate, edate;
	mio::IOUtils::convertString(sdate, argv[1], TZ);
	mio::IOUtils::convertString(edate, argv[2], TZ);
	const bool has_data = check_sampling_rate(io_out, sdate, edate); //info for solar resampling and sanity check
	if (!has_data)
		return 1;
	std::cout << "Performing spatial interpolations..." << std::endl;
	
	//use MeteoIO's date calculations to populate a vector with all days:
	std::vector<mio::Date> days;
	mio::Date dt(sdate);
	while (dt < edate + 1) {
		days.push_back(dt);
		dt += 1;
	}
	mio::Grid2DObject sum_iswr(dem, 0.); //copy raster info and set to zero 
	int missing_counter = 0;
	std::mutex missing_counter_lock;
	std::mutex sum_iswr_lock;

	std::streambuf *oldbuf(std::cerr.rdbuf()); //for each time step we get interpolation info from MeteoIO
	std::stringstream ss; //we save this to a stream to discard some messages
	std::cerr.rdbuf(ss.rdbuf());

	std::for_each(std::execution::seq, days.begin(), days.end(), [&](auto&& dt) {
		//TODO: why does par_unseq fail from a cronjob?
		mio::IOManager io_par(cfg); //one i/o instance per worker
		int year, month, day;
		dt.getDate(year, month, day);
		for (size_t ii = 0; ii < hours.size(); ++ii) { //run through times of the day
			dt.setDate(year, month, day, hours[ii], 0, TZ);
			try {
				mio::Grid2DObject grid_iswr;
				io_par.getMeteoData(dt, dem, mio::MeteoData::ISWR, grid_iswr); //interpolation
				sum_iswr_lock.lock();
				sum_iswr += grid_iswr;
				sum_iswr_lock.unlock();
			} catch (const mio::IOException& e) {
				missing_counter_lock.lock();
				if (missing_counter == 0)
					std::cout << "[W] No good data on " << dt.toString(mio::Date::ISO) <<
					    " (no further warnings will be shown)." << std::endl;
				missing_counter++;
				missing_counter_lock.unlock();
			}
		}
	});
	std::cerr.rdbuf(oldbuf); //restore output to cerr

	io_out.write2DGrid(sum_iswr, mio::MeteoGrids::ISWR, edate); //name file according to last step

	if (missing_counter > 0)
		std::cerr << "[W] " << missing_counter << " time steps were lacking data." << std::endl;
	std::string err;
	while (getline(ss, err)) { //print encountered warnings after all is done
		if (!err.rfind("[i] Interpolating", 0) == 0) //... except known unwanted info
			std::cerr << err;
	}
	std::cout << __FILE__ << " done" << std::endl;
	return 0;
}

