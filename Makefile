################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
# This Makefile compiles the MeteoIO control program

CC         = g++ -std=c++17
CFLAGS     = -Wall -Wextra
DEBUG      = -g
# MeteoIO must be installed for the current user:
METEOIODIR = $(shell echo ~/.local)

ENV      = -Wl,-rpath=$(METEOIODIR)
LIBS     = -rdynamic -lstdc++ -lmeteoio -ldl -ltbb
INCLUDE  = -I. -I$(METEOIODIR)/include
LIBDIR   = -L$(METEOIODIR)/lib
BUILDDIR = build

#####################
#	RULES
#####################
%.o: %.cc
	$(CC) $(DEBUG) $(CFLAGS) -o $(BUILDDIR)/$@ -c $< $(INCLUDE)

#####################
#	TARGETS
#####################
all: directories solar_sum

directories:
	mkdir -p $(BUILDDIR)
solar_sum: solar_sum.o
	$(CC) $(ENV) $(DEBUG) $(LIBDIR) -o $(BUILDDIR)/$@ $(BUILDDIR)/$@.o ${LIBS}
clean:
	rm -rf build/*.o build/solar_sum
