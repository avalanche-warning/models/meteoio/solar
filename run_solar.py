#!/usr/bin/env python3

################################################################################
# Copyright 2022 alpine-software.at                                            #
################################################################################
# This is free software you can redistribute/modify under the terms of the     #
# GNU Affero General Public License 3 or later: http://www.gnu.org/licenses    #
################################################################################
#This script runs the full solar sum model, i. e. it downloads data, starts
#the simulation and plots the results.

import awio
from awgeo import get_epsg_code
from awset.domain_settings import get, exists

from datetime import datetime, timedelta
import glob
import os
import sys
import subprocess

def run_domain(sdate: str, edate: str, domain: str, plot_pdf: bool=False):
    # Prepare list of stations to run and download data for them:
    stations = ["solar", "meteo", "stations"]
    stations = get(stations, domain)
    meteo_dl_dir = os.path.expanduser("~snowpack/solar/data")
    try:
        os.remove(meteo_dl_dir + "/*.smet")
    except OSError:
        pass
    download_script_path = os.path.expanduser("~data/query/wiski/wiski_web.py")
    subprocess.call(["python3", download_script_path, stations, meteo_dl_dir])

    # Update solar model's ini file with info from our domain settings file:
    ini_template = "./input/io.ini.template"
    with open(ini_template, "r") as file :
        demfile_xml = get(["domain", "dem", "file"], domain)
        dem_path, dem_ext = os.path.splitext(demfile_xml)

        demfile_asc =  dem_path + ".asc"
        data = file.read()
        data = data.replace("$DOMAIN", domain)
        data = data.replace("$DEMFILE", demfile_asc)
        epsg = get_epsg_code(f"{dem_path}.prj")
        if not epsg:
            print(r'[E] Could not determine EPSG code needed for raster output. You can e. g. specify it in the file name by putting "epsg#####" at some place.')
            sys.exit()
        data = data.replace("$EPSG", str(epsg))

        with open("./input/io.ini", "w") as outfile:
            outfile.write(f"# This config file was auto-created by '{__file__}' and can be deleted, but will be re-created.\n")
            outfile.write(data)

    # Clear output:
    os.makedirs(f"./output/{domain}", exist_ok=True)
    filelist = awio.multiglob(f"./output/{domain}/*.asc", f"./output/{domain}/*.pdf", f"./output/{domain}/*.prj")
    for file in filelist:
        os.remove(file)

    # Run calculation powered by MeteoIO's solar engine:
    mio_env = os.environ.copy()
    mio_env["LD_LIBRARY_PATH"] = os.path.expanduser("~snowpack/.local/lib")
    sim_prog_path = "./solar_sum"
    print(f"Calling '{sim_prog_path}' with dates {sdate} to {edate}...")
    subprocess.call([sim_prog_path, sdate, edate], env=mio_env)

    if plot_pdf: # plot results to pdf and upload
        file_edate = edate.replace(":", ".") # date format as used by MeteoIO grid output
        grid = f"./output/{domain}/{file_edate}_ISWR.asc"
        plot_prog_path = "./plot_solar.R"
        print(f"Calling 'Rscript plot_solar.R' with file '{grid}'...")
        subprocess.call(["Rscript", plot_prog_path, grid, sdate, edate], env=mio_env)

        # Upload pdf:
        if exists(["solar", "output", "upload"], domain):
            host = get(["solar", "output", "upload", "host"], domain)
            output_prog_path = os.path.expanduser("~opera/upload/upload.py")
            pdffiles = glob.glob(os.path.expanduser(f"~snowpack/solar/output/{domain}/*.pdf"))
            for pdf in pdffiles:
                print(f"[i] Uploading '{pdf}'...")
                subprocess.call(["python3", output_prog_path, pdf, host])

    print(__file__ + " done")

if __name__ == "__main__":
    # defaults:
    plot_pdf=False
    today = datetime.now() - timedelta(hours = 2)
    dt_format = "%Y-%m-%dT%H:%M:%S"
    edate = today.strftime(dt_format)
    calc_days = 7
    sdate = (today - timedelta(days=calc_days)).strftime(dt_format)

    # user input:
    if len(sys.argv) > 4:
        sys.exit("[E] Synopsis: python3 run_solar.py [<start_date> <end_date>] [<plot_pdf>]")
    if len(sys.argv) == 2:
        domain = sys.argv[1]
    elif 2 < len(sys.argv) < 5:
        sdate = sys.argv[1]
        edate = sys.argv[2]
    if len(sys.argv) == 4:
        plot_pdf = sys.argv[3]
    run_domain(sdate, edate, domain, plot_pdf)
